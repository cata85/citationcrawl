import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Element;


public class CitationCrawl {
    public static void main(String[] args) throws IOException {
        String input_path = "/Users/Carter/Desktop/output";
        String output_path = "/Users/Carter/Desktop";
        String filename = output_path + "/urls.txt";
        File folder = new File(input_path);
        File[] folder_names = folder.listFiles();
        assert folder_names != null;
        String[] file_names = new String[folder_names.length];

        for(int i=0;i<folder_names.length;i++) {
            file_names[i] = folder_names[i].toString() + "/" + folder_names[i].getName() + ".json";
            try {
                JSONArray dirty_references = get_references(file_names[i]);
                ArrayList<String> split_references = split_references(dirty_references);
                ArrayList<String> relevant_citation = get_relevant_citation(split_references);
                ArrayList<String> urls = google(relevant_citation);
                toText(urls, filename);
                //urls.forEach(System.out::println);
            } catch (Exception e) {
                System.out.println("File Error: " + file_names[i]);
            }
            System.out.println();
        }
    }


    /**
     * Opens specified json file and returns the references in that json
     * @param file_name The file location for the json file
     * @return A JSON array of all the references in that file
     * @throws Exception
     */
    private static JSONArray get_references(String file_name) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(file_name));
        JSONObject multiline_word_fix_text = (JSONObject) jsonObject.get("multiline_word_fix_text");
        JSONObject extracted_sections = (JSONObject) multiline_word_fix_text.get("extracted_sections");

        return((JSONArray) extracted_sections.get("references"));
    }


    /**
     * Takes the messy references and cleans them up
     * @param dirty_references The messy version of the references
     * @return A cleaned up version of the references by reference number
     */
    private static ArrayList<String> split_references(JSONArray dirty_references) {
        ArrayList<String> split_references = new ArrayList<>();
        int reference_number = 1;
        String references = convert_to_string(dirty_references);
        String reference = "1" + references.substring(1,3);

        try {
            if (!references.substring(0, 1).equals(Integer.toString(reference_number))) {
                split_references.add("None");
                return (split_references);
            }
        } catch (Exception e) {
            split_references.add("None");
            return(split_references);
        }
        for(int i=3;i<references.length();i++) {
            if(check_for_num(references.substring(i-2, references.length()-1), reference_number)) {
                split_references.add(reference);
                reference_number++;
                reference = Integer.toString(reference_number);
                if(reference_number > 9) {
                    i++;
                }
            } else {
                reference += references.substring(i, i+1);
            }
        }

        return(split_references);
    }


    /**
     * Takes the JSONArray of unsorted references and converts them to a string for easier handling
     * @param dirty_references JSONArray of the unsorted references
     * @return A string of the unsorted references
     */
    private static String convert_to_string(JSONArray dirty_references) {
        String references = "";

        for(Object word : dirty_references) {
            references += word + " ";
        }

        return(references);
    }


    /**
     * Takes a small section from references to see if its the beginning of a new reference
     * @param section The subsection of the references wanting to be checked
     * @param count The current reference count
     * @return Whether the subsection is a new reference
     */
    private static boolean check_for_num(String section, int count) {
        String string_count = Integer.toString(count + 1);
        int length = string_count.length();

        try {
            if (section.substring(0, 1).equals(".") && section.substring(2, 2 + length).equals(string_count)) {
                if (section.substring(2 + length, 3 + length).equals(" ")) {
                    return (true);
                }
            }
        } catch (Exception e) {
            return(false);
        }

        return(false);
    }


    /**
     * Takes references and returns the needed section for finding a pdf on google
     * @param split_references Takes array list of split up references
     * @return returns only the relevant part of each citation needed for searching for the pdf
     */
    private static ArrayList<String> get_relevant_citation(ArrayList<String> split_references) {
        ArrayList<String> relevant_citation = new ArrayList<>();

        for(String citation : split_references) {
            if(!citation.equals("None")) {
                String[] temp = citation.split("\\.");
                String temp_relevant_citation = "";
                int relevant_parts = temp.length-1;
                try {
                    while (!last_uppercase(temp[relevant_parts - 1].replace(" ", ""))) {
                        relevant_parts--;
                    }
                    for (int i = relevant_parts; i < temp.length; i++) {
                        temp_relevant_citation += temp[i];
                    }
                    relevant_citation.add(temp_relevant_citation);
                } catch (Exception e) {
                    relevant_citation.add("None"); // Can be improved to where it doesn't exclude, but its minimal rn
                }
            } else {
                relevant_citation.add(citation);
            }
        }

        return(relevant_citation);
    }


    /**
     * Takes a given word (or words) and determines if the last character is uppercase
     * @param word_to_check The word passed in to check
     * @return Whether the last character is uppercase
     */
    private static boolean last_uppercase(String word_to_check) {
        int length = word_to_check.length();
        char last_letter = word_to_check.charAt(length-1);

        return(Character.isUpperCase(last_letter));
    }


    /**
     * Takes relevant citations and returns the urls obtained from a google search result
     * @param relevant_citation Takes an array list of relevant citations
     * @throws IOException
     * @return An arraylist of the relevant urls
     */
    private static ArrayList<String> google(ArrayList<String> relevant_citation) throws IOException {
        String google = "http://www.google.com/search?q=";
        String userAgent = "ExampleBot 1.0 (+http://example.com/bot)"; // Change this to your company's name and bot homepage!
        ArrayList<String> urls = new ArrayList<>();

        for(String citation : relevant_citation) {
            if(!citation.equals("None")) {
                Elements links = Jsoup.connect(google + URLEncoder.encode(citation, "UTF-8")).userAgent(userAgent).get().select(".g>.r>a");
                for(int i=0;i<1;i++) {
                    Element link = links.get(i);
                    String url = link.absUrl("href");
                    url = URLDecoder.decode(url.substring(url.indexOf('=') + 1, url.indexOf('&')), "UTF-8");

                    if (!url.startsWith("http")) {
                        continue; // Ads/news/etc.
                    }
                    System.out.println(url);
                    urls.add(url);
                }
            } else {
                System.out.println("None");
                urls.add("None");
            }
        }

        return(urls);
    }


    /**
     * Appends the arraylist of urls to a text file
     * @param urls
     * @param filename
     */
    public static void toText(ArrayList<String> urls, String filename) {
        try {
            FileWriter fileWriter = new FileWriter(filename, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for(String url : urls) {
                if(!url.equals("None")) {
                    bufferedWriter.write(url + "\n");
                }
            }
            bufferedWriter.close();
        } catch (IOException x) {
            System.out.println("Writing error");
        }
    }
}
